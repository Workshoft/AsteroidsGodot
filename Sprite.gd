extends Sprite

# class member variables go here, for example:


func _ready():

	pass

func _process(delta):
	
	if(Input.is_action_pressed("up")):
		
		self.position.y -= 5
	
	if(Input.is_action_pressed("down")):
		self.position.y += 5
		
	if(Input.is_action_pressed("right")):
		self.position.x += 5
		
	if(Input.is_action_pressed("left")):
		self.position.x -= 5
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	
	var mouseX = get_viewport().get_mouse_position()[0]
	var mouseY = get_viewport().get_mouse_position()[1]
	
	var vx = mouseX - self.position.x 
	var vy = mouseY - self.position.y

	var v = Vector2(vx, vy).normalized()
	var u = Vector2(0, -1).normalized()
	
	self.set_rotation(u.angle_to(v))
	
	
	
	
	
	
	